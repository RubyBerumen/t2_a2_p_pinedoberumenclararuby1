'''
Created on 9 oct. 2020

@author: Ruby
'''

class CadenaInversa:
    
    def invertirCadena (self, cadena, i, res):
        
        if(i<len(cadena)):
            res = cadena[i]+res
            i+=1
            self.invertirCadena(CadenaInversa,cadena,i,res);
        else:
            print("Cadena inversa : "+ res);


cadena = str(input("Ingresa una cadena de caracteres para mostrarla invertida:"))
CadenaInversa.invertirCadena(CadenaInversa, cadena, 0, "")
    