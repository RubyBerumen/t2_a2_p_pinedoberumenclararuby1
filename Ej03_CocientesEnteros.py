'''
Created on 9 oct. 2020

@author: Ruby
'''
class Cocientes:
    
    def cocientes(self, dividendo, divisor, x, residuo):
        divisor1=str(divisor)
        if(len(divisor1)<=len(dividendo)):
            if(x<=(len(dividendo)-len(divisor1))):
                lnf=x+len(divisor1)
                div=int(dividendo[x:lnf])
                div = div + (residuo*10)
                print("Cociente no. "+str(x+1)+": ",end="")
                print(int(div/divisor))
                residuo=div%divisor
                subC=dividendo[0:x]
                
                for i in range(x,lnf):
                    subC+='0'
                subC+=dividendo[lnf:len(dividendo)]
                dividendo=subC
                x+=1;
                self.cocientes(Cocientes,dividendo,divisor,x,residuo)
            else:
                print("Residuo: "+str(residuo))
        else:
            print("No tiene cocientes enteros")


dividendo = str(input("Ingresa el dividendo: "))
divisor = int (input("Ingresa el divisor: "))
Cocientes.cocientes(Cocientes,dividendo, divisor, 0, 0)
    